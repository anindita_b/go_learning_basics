package main

import "fmt"

func main() {

	var myarr [2]string
	myarr[0] = "GFG"
	myarr[1] = "GeekForGeeks"

	fmt.Println("The array elements are as followed:")
	fmt.Println("Element 1:", myarr[0])
	fmt.Println("Element 2:", myarr[1])

	//using shorthand notation

	arr := [4]string{"Apple", "Google", "MSFT", "AMZN"}

	fmt.Println("The elements of the array")

	for i := 0; i <= 3; i++ {
		fmt.Println(arr[i])
	}

}
