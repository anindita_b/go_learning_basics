package main

import "fmt"

func main() {
	var x int = 0

	//Label1:
	for x < 8 {
		if x == 5 {
			//using goto statememt

			x = x + 1
			continue
		}

		fmt.Println("value is %d\n", x)
		x++
	}
}
