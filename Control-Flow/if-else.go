package main

import "fmt"

func main() {

	//taking local variables

	var a int = 100
	var b int = 201

	// using if statements

	if a%2 == 0 {
		fmt.Println("Even number")
	}
	if b%2 == 0 {
		fmt.Println("Odd Number")
	} else {
		fmt.Println("The number is invalid")
	}

}
