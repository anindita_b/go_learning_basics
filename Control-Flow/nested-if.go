package main

import "fmt"

func main() {
	var v1 int = 400
	var v2 int = 750

	//using if statements

	if v1 == 400 {
		if v2 == 700 {

			fmt.Println("Value of v1 is 400 & vs is 700")
		}
	}

	// Check in nested if conditions

	if v1 == 100 {
		fmt.Println("Value of v1 is 100")
	} else if v1 == 300 {
		fmt.Println("The value of v1 is 300")

	} else {
		fmt.Println("None of the values matching")
	}

}
