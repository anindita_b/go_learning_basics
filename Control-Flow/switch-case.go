package main

import "fmt"

func main() {
	var language string = "java"

	switch language {
	case "C":
		{
			fmt.Println("The language is C")
		}
	case "Python":
		{
			fmt.Println("The language is Python")
		}
	case "Golang":
		{
			fmt.Println("The language is Golang")
		}
	case "java", "J2EE":
		{
			fmt.Println("The language is Java/J2EE")
		}
	}
}
