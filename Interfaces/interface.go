package main

import "fmt"

type tank interface {

	//Methods

	Area() float64
	Volume() float64
}

//Struct

type myvalue struct {
	radius float64
	height float64
}

// Implementing methods of the tank interface

func (m myvalue) Area() float64 {
	return 2*m.radius*m.height + 2*3.14*m.radius*m.radius
}

func (m myvalue) Volume() float64 {
	return 3.14 * m.radius * m.radius * m.height
}

func main() {
	var t tank
	t = myvalue{10, 14}
	fmt.Println("The Area of the interface is", t.Area())
	fmt.Println("The volume of the interface is", t.Volume())

}
