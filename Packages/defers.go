package main

import "fmt"

func multiplication(a float64) float64 {
	pi := 3.14
	perimeter := 2 * pi * a
	fmt.Println("The result", perimeter)
	return perimeter
}

func results() {
	fmt.Println("To display the results using Defer functions!")
}

func main() {

	multiplication(23.45)

	defer multiplication(67.399)

	results()

	fmt.Println("The results are already displayed")
}
