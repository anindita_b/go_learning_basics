package main

import "fmt"

func main() {

	var x int = 5678

	var p *int = &x

	fmt.Println("The value stored in x is ", x)
	fmt.Printf("The address of x is %v\n", &x)
	fmt.Printf("The value stored in p is %v\n", p)

}
