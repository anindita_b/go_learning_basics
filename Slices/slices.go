package main

import "fmt"

func main() {

	arr := [6]string{"This", "is", "Friday", "Evening", "with", "Golang"}

	// display the array

	fmt.Println("Array:", arr)

	//Creating the slice

	myslice := arr[1:5]

	// Display the slice

	fmt.Println("The slice display:", myslice)

	//The length of the slice

	fmt.Println("Length of the slice: %d", len(myslice))

	// Display the capacity of the slice

	fmt.Println("\n the capacity of the slice: %d", cap(myslice))
}
