package main

import "fmt"

// Defining the struct type

type Address struct {
	Name    string
	city    string
	Pincode int
}

func main() {

	var addr Address
	addr = Address{Name: "Andrei"}
	fmt.Println(addr)

	a1 := Address{"Adrian", "Basinghall", 56342}

	fmt.Println("Address1 is:", a1)

	// Naming fields with struct

	a2 := Address{Name: "Antonio", city: "Amsterdam", Pincode: 22334}
	fmt.Println("The Address2 is", a2)
}
