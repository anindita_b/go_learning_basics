package main

import "fmt"

func main() {
	var X uint8 = 225
	fmt.Println(X+1, X)

	//using 16-bit signed int

	var Y int16 = 32767
	fmt.Println(Y+2, Y-2)

	a := 20.45
	b := 34.89

	var m complex128 = complex(6, 2)
	var n complex64 = complex(9, 2)

	c := b - a
	fmt.Printf("The result is %f\n", c)

	// Display the type of C variable

	fmt.Printf("The type of C is :%T\n", c)

	fmt.Println(m)
	fmt.Println(n)

	fmt.Printf("The type of m is %T and "+"the type of n is %T", m, n)

}
