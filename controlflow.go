package main

import "fmt"

func main() {
	var a int = 100
	var b int = 275

	if a%2 == 0 {
		fmt.Println("even number\n")
	}
	if b%2 == 0 {
		fmt.Println("even number")
	} else {
		fmt.Println("Odd number")
	}

}
