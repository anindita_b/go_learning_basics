package main

import "fmt"

var a int = 20

func main() {
	// declare the local formal parameter

	var a int = 5
	var b int = 30
	var c int = 50

	fmt.Printf("value of the variable a %d\n", a)
	fmt.Printf("The value of the variable b is %d\n,", b)
	fmt.Printf("The value of the variable c is %d\n", c)

	c = sum(a, b)
	fmt.Printf("The sum of the value is %d\n", c)

}

func sum(a, b int) int {
	fmt.Printf("value of a is %d\n", a)
	fmt.Printf("The value of b is %d\n", b)
	return a + b
}
