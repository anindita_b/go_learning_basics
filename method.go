package main

import "fmt"

type Author struct {
	name      string
	branch    string
	class     string
	particles int
	salary    int
}

// Method with Receiver of Author type

func (a Author) show() {
	fmt.Println("The Author's name", a.name)
	fmt.Println("The Branch is ", a.branch)
	fmt.Println("The Class is ", a.class)
	fmt.Println("The Particles is", a.particles)
	fmt.Println("The Salary is", a.salary)
}

// Main function

func main() {
	res := Author{
		name:      "Andrei",
		branch:    "CS",
		class:     "101",
		particles: 203,
		salary:    45000,
	}
	// calling the method
	res.show()
}
