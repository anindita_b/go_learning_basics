package main

import "fmt"

func main() {
	str1 := "GeeksForGeeks"
	str2 := "geeks4geeks"
	result1 := str1 == str2

	//display the result
	fmt.Println(result1)

	fmt.Printf("This type of result is %T\n", result1)

	str := "GeeksforNewGeeks"

	fmt.Printf("The length of the string is %s\n", len(str))

	fmt.Printf("\n The type of variable is %T\n", str)

}
