package main

import "fmt"

func main() {
	var mynewvariable1 = 20

	fmt.Println("The value of the variable is", mynewvariable1)
	fmt.Printf("The type of the variable is %T\n", mynewvariable1)
}
